import re
import json
import os

#Create output of klar's analysis
os.system("sudo CLAIR_ADDR=localhost REGISTRY_INSECURE=true /root/go/bin/klar localhost:5000/test:latest > clair-output.txt 2>/dev/null")


#Open files and save them in variables
with open ("clair-output.txt","r") as clair:
    data=clair.readlines()

with open("tolerates.cfg","r") as tolerates:
    tolerate=tolerates.readlines()

with open("values.json") as values:
    value= json.load(values)

#Counters
medium = 0
low = 0
unknown = 0
high = 0

#Border of tolerates errors in analysis
medium_line = value["medium"]
high_line = value["high"]
low_line = value["low"]
unknown_line = value["unknown"]

#Total of errors founds in analysis
for line in data:
    if line.startswith("Medium:"):
        medium = int(line.strip("\n").strip("Medium: "))
    elif line.startswith("Unknown:"):
        unknown = int(line.strip("\n").strip("Unknown: "))
    elif line.startswith("Low:"):
        low = int(line.strip("\n").strip("Low: "))
    elif line.startswith("High:"):
        high = int(line.strip("\n").strip("High: "))

lista_cve=[]

#Save all CVE in array
for line in data:
    if line.startswith('CVE'):
        lista_cve.append(line)

#For each tolerate in the file if this are present in array substract one of the counters and remove this.
for tol in tolerate:
    for cve in lista_cve:
        if cve.startswith(tol.strip("\n")):
            if cve.endswith("[Medium]\n"):
                medium -= 1
            elif cve.endswith("[Low]\n"):
                low -= 1
            elif cve.endswith("[High]\n"):
                high -= 1
            elif cve.endswith("[Unknown]\n"):
                unknown -= 1
            lista_cve.remove(cve)



flag = True

#If some condition is not passed the test will fail

if medium > medium_line and medium_line >= 0:
    flag = False
elif low > low_line and low_line >= 0:
    flag = False
elif unknown > unknown_line and unknown_line >= 0:
    flag = False
elif high > high_line and high_line >= 0:
    flag = False

#If test fails, show the cve
if flag == True:
    print "Vulnerabilities not found"
    exit(0)
else:
    for cve in lista_cve:
        print(cve.strip("\n").strip(":"))
    exit(1)
